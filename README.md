# BREATHE

## Notes

The app is not a working prototype.
I've never made a Ruby application, and the recruiter folks told me this was supposed to take me only 2 hours,
and that I should submit it ASAP... So this is where it's at. 
I don't want to be dishonest about it and stall or anything.

I did choose to set it up with docker tools, so hopefully that's at least interesting to look at.
I'd love to chat about it. Or let me know if you ever need a Backbone.Marionette expert. mailto:adamowirth@gmail.com


## Setup

You'll need Docker:

- Linux: https://docs.docker.com/get-started/
- Windows and Mac: https://www.docker.com/products/docker-desktop

Then, from the root directory, run this (& wait a bit):
`docker-compose up`

Navigate to `http://localhost:8080/` to see the application.

## Config

In theory, copy `env.example` in the root, and create `.env` for yourself to edit.

In practice, it's not used right now.

## References

I put my physical notes as scanned PDFs, as well as an action plan I was launching from, in the folder `./reference`.

