require "application_system_test_case"

class PeakFlowsTest < ApplicationSystemTestCase
  setup do
    @peak_flow = peak_flows(:one)
  end

  test "visiting the index" do
    visit peak_flows_url
    assert_selector "h1", text: "Peak Flows"
  end

  test "creating a Peak flow" do
    visit peak_flows_url
    click_on "New Peak Flow"

    fill_in "Rating", with: @peak_flow.rating
    click_on "Create Peak flow"

    assert_text "Peak flow was successfully created"
    click_on "Back"
  end

  test "updating a Peak flow" do
    visit peak_flows_url
    click_on "Edit", match: :first

    fill_in "Rating", with: @peak_flow.rating
    click_on "Update Peak flow"

    assert_text "Peak flow was successfully updated"
    click_on "Back"
  end

  test "destroying a Peak flow" do
    visit peak_flows_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Peak flow was successfully destroyed"
  end
end
