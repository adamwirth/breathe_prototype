require "application_system_test_case"

class MedicinePlansTest < ApplicationSystemTestCase
  setup do
    @medicine_plan = medicine_plans(:one)
  end

  test "visiting the index" do
    visit medicine_plans_url
    assert_selector "h1", text: "Medicine Plans"
  end

  test "creating a Medicine plan" do
    visit medicine_plans_url
    click_on "New Medicine Plan"

    fill_in "Dosagemeasure", with: @medicine_plan.dosageMeasure
    fill_in "Dosagenum", with: @medicine_plan.dosageNum
    fill_in "Frequency", with: @medicine_plan.frequency
    fill_in "Name", with: @medicine_plan.name
    click_on "Create Medicine plan"

    assert_text "Medicine plan was successfully created"
    click_on "Back"
  end

  test "updating a Medicine plan" do
    visit medicine_plans_url
    click_on "Edit", match: :first

    fill_in "Dosagemeasure", with: @medicine_plan.dosageMeasure
    fill_in "Dosagenum", with: @medicine_plan.dosageNum
    fill_in "Frequency", with: @medicine_plan.frequency
    fill_in "Name", with: @medicine_plan.name
    click_on "Update Medicine plan"

    assert_text "Medicine plan was successfully updated"
    click_on "Back"
  end

  test "destroying a Medicine plan" do
    visit medicine_plans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Medicine plan was successfully destroyed"
  end
end
