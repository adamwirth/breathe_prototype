require "application_system_test_case"

class UserCarePlansTest < ApplicationSystemTestCase
  setup do
    @user_care_plan = user_care_plans(:one)
  end

  test "visiting the index" do
    visit user_care_plans_url
    assert_selector "h1", text: "User Care Plans"
  end

  test "creating a User care plan" do
    visit user_care_plans_url
    click_on "New User Care Plan"

    fill_in "Emergencycontacts", with: @user_care_plan.emergencyContacts
    fill_in "Peakflowhistory", with: @user_care_plan.peakFlowHistory
    fill_in "Severityclassification", with: @user_care_plan.severityClassification
    fill_in "Triggerlist", with: @user_care_plan.triggerList
    fill_in "Zones", with: @user_care_plan.zones
    click_on "Create User care plan"

    assert_text "User care plan was successfully created"
    click_on "Back"
  end

  test "updating a User care plan" do
    visit user_care_plans_url
    click_on "Edit", match: :first

    fill_in "Emergencycontacts", with: @user_care_plan.emergencyContacts
    fill_in "Peakflowhistory", with: @user_care_plan.peakFlowHistory
    fill_in "Severityclassification", with: @user_care_plan.severityClassification
    fill_in "Triggerlist", with: @user_care_plan.triggerList
    fill_in "Zones", with: @user_care_plan.zones
    click_on "Update User care plan"

    assert_text "User care plan was successfully updated"
    click_on "Back"
  end

  test "destroying a User care plan" do
    visit user_care_plans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User care plan was successfully destroyed"
  end
end
