require "application_system_test_case"

class PlanZonesTest < ApplicationSystemTestCase
  setup do
    @plan_zone = plan_zones(:one)
  end

  test "visiting the index" do
    visit plan_zones_url
    assert_selector "h1", text: "Plan Zones"
  end

  test "creating a Plan zone" do
    visit plan_zones_url
    click_on "New Plan Zone"

    fill_in "Color", with: @plan_zone.color
    fill_in "Controlmedicineplan", with: @plan_zone.controlMedicinePlan
    fill_in "Description", with: @plan_zone.description
    fill_in "Peakflowmax", with: @plan_zone.peakflowMax
    fill_in "Peakflowmin", with: @plan_zone.peakflowMin
    fill_in "Physicalactivityplan", with: @plan_zone.physicalActivityPlan
    fill_in "Quickmedicineplan", with: @plan_zone.quickMedicinePlan
    fill_in "Symptoms", with: @plan_zone.symptoms
    click_on "Create Plan zone"

    assert_text "Plan zone was successfully created"
    click_on "Back"
  end

  test "updating a Plan zone" do
    visit plan_zones_url
    click_on "Edit", match: :first

    fill_in "Color", with: @plan_zone.color
    fill_in "Controlmedicineplan", with: @plan_zone.controlMedicinePlan
    fill_in "Description", with: @plan_zone.description
    fill_in "Peakflowmax", with: @plan_zone.peakflowMax
    fill_in "Peakflowmin", with: @plan_zone.peakflowMin
    fill_in "Physicalactivityplan", with: @plan_zone.physicalActivityPlan
    fill_in "Quickmedicineplan", with: @plan_zone.quickMedicinePlan
    fill_in "Symptoms", with: @plan_zone.symptoms
    click_on "Update Plan zone"

    assert_text "Plan zone was successfully updated"
    click_on "Back"
  end

  test "destroying a Plan zone" do
    visit plan_zones_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Plan zone was successfully destroyed"
  end
end
