require "test_helper"

class MedicinePlansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @medicine_plan = medicine_plans(:one)
  end

  test "should get index" do
    get medicine_plans_url
    assert_response :success
  end

  test "should get new" do
    get new_medicine_plan_url
    assert_response :success
  end

  test "should create medicine_plan" do
    assert_difference('MedicinePlan.count') do
      post medicine_plans_url, params: { medicine_plan: { dosageMeasure: @medicine_plan.dosageMeasure, dosageNum: @medicine_plan.dosageNum, frequency: @medicine_plan.frequency, name: @medicine_plan.name } }
    end

    assert_redirected_to medicine_plan_url(MedicinePlan.last)
  end

  test "should show medicine_plan" do
    get medicine_plan_url(@medicine_plan)
    assert_response :success
  end

  test "should get edit" do
    get edit_medicine_plan_url(@medicine_plan)
    assert_response :success
  end

  test "should update medicine_plan" do
    patch medicine_plan_url(@medicine_plan), params: { medicine_plan: { dosageMeasure: @medicine_plan.dosageMeasure, dosageNum: @medicine_plan.dosageNum, frequency: @medicine_plan.frequency, name: @medicine_plan.name } }
    assert_redirected_to medicine_plan_url(@medicine_plan)
  end

  test "should destroy medicine_plan" do
    assert_difference('MedicinePlan.count', -1) do
      delete medicine_plan_url(@medicine_plan)
    end

    assert_redirected_to medicine_plans_url
  end
end
