require "test_helper"

class PeakFlowsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @peak_flow = peak_flows(:one)
  end

  test "should get index" do
    get peak_flows_url
    assert_response :success
  end

  test "should get new" do
    get new_peak_flow_url
    assert_response :success
  end

  test "should create peak_flow" do
    assert_difference('PeakFlow.count') do
      post peak_flows_url, params: { peak_flow: { rating: @peak_flow.rating } }
    end

    assert_redirected_to peak_flow_url(PeakFlow.last)
  end

  test "should show peak_flow" do
    get peak_flow_url(@peak_flow)
    assert_response :success
  end

  test "should get edit" do
    get edit_peak_flow_url(@peak_flow)
    assert_response :success
  end

  test "should update peak_flow" do
    patch peak_flow_url(@peak_flow), params: { peak_flow: { rating: @peak_flow.rating } }
    assert_redirected_to peak_flow_url(@peak_flow)
  end

  test "should destroy peak_flow" do
    assert_difference('PeakFlow.count', -1) do
      delete peak_flow_url(@peak_flow)
    end

    assert_redirected_to peak_flows_url
  end
end
