require "test_helper"

class UserCarePlansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_care_plan = user_care_plans(:one)
  end

  test "should get index" do
    get user_care_plans_url
    assert_response :success
  end

  test "should get new" do
    get new_user_care_plan_url
    assert_response :success
  end

  test "should create user_care_plan" do
    assert_difference('UserCarePlan.count') do
      post user_care_plans_url, params: { user_care_plan: { emergencyContacts: @user_care_plan.emergencyContacts, peakFlowHistory: @user_care_plan.peakFlowHistory, severityClassification: @user_care_plan.severityClassification, triggerList: @user_care_plan.triggerList, zones: @user_care_plan.zones } }
    end

    assert_redirected_to user_care_plan_url(UserCarePlan.last)
  end

  test "should show user_care_plan" do
    get user_care_plan_url(@user_care_plan)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_care_plan_url(@user_care_plan)
    assert_response :success
  end

  test "should update user_care_plan" do
    patch user_care_plan_url(@user_care_plan), params: { user_care_plan: { emergencyContacts: @user_care_plan.emergencyContacts, peakFlowHistory: @user_care_plan.peakFlowHistory, severityClassification: @user_care_plan.severityClassification, triggerList: @user_care_plan.triggerList, zones: @user_care_plan.zones } }
    assert_redirected_to user_care_plan_url(@user_care_plan)
  end

  test "should destroy user_care_plan" do
    assert_difference('UserCarePlan.count', -1) do
      delete user_care_plan_url(@user_care_plan)
    end

    assert_redirected_to user_care_plans_url
  end
end
