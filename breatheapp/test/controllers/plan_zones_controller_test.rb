require "test_helper"

class PlanZonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_zone = plan_zones(:one)
  end

  test "should get index" do
    get plan_zones_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_zone_url
    assert_response :success
  end

  test "should create plan_zone" do
    assert_difference('PlanZone.count') do
      post plan_zones_url, params: { plan_zone: { color: @plan_zone.color, controlMedicinePlan: @plan_zone.controlMedicinePlan, description: @plan_zone.description, peakflowMax: @plan_zone.peakflowMax, peakflowMin: @plan_zone.peakflowMin, physicalActivityPlan: @plan_zone.physicalActivityPlan, quickMedicinePlan: @plan_zone.quickMedicinePlan, symptoms: @plan_zone.symptoms } }
    end

    assert_redirected_to plan_zone_url(PlanZone.last)
  end

  test "should show plan_zone" do
    get plan_zone_url(@plan_zone)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_zone_url(@plan_zone)
    assert_response :success
  end

  test "should update plan_zone" do
    patch plan_zone_url(@plan_zone), params: { plan_zone: { color: @plan_zone.color, controlMedicinePlan: @plan_zone.controlMedicinePlan, description: @plan_zone.description, peakflowMax: @plan_zone.peakflowMax, peakflowMin: @plan_zone.peakflowMin, physicalActivityPlan: @plan_zone.physicalActivityPlan, quickMedicinePlan: @plan_zone.quickMedicinePlan, symptoms: @plan_zone.symptoms } }
    assert_redirected_to plan_zone_url(@plan_zone)
  end

  test "should destroy plan_zone" do
    assert_difference('PlanZone.count', -1) do
      delete plan_zone_url(@plan_zone)
    end

    assert_redirected_to plan_zones_url
  end
end
