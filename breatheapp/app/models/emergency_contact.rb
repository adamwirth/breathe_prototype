class EmergencyContact < ApplicationRecord
    belongs_to :user_care_plan
    
    validates :name, presence: true
    validates :phone, presence: true
end
