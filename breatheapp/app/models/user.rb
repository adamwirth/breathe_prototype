class User < ApplicationRecord
    # has_many :patients # todo not sure how to do a recursive user id, just doing integers for now
    has_one :user_care_plan, dependent: :destroy
    
    validates :name, presence: true
end
