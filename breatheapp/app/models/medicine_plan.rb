class MedicinePlan < ApplicationRecord
    belongs_to :plan_zone
    
    validates :dosage_num, presence: true
    validates :dosage_measure, presence: true
    
    # validates :frequency, presence: true # todo i would like to set a default to "as needed"
    # todo change frequency setup, frankly
end
