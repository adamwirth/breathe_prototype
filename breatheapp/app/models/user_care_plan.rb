class UserCarePlan < ApplicationRecord
    has_many :peak_flow, dependent: :destroy
    has_many :plan_zone, dependent: :destroy
    has_many :emergency_contacts, dependent: :destroy
    
    validates :severity_confirmation, presence: true
end
