class PlanZone < ApplicationRecord
    belongs_to :user_care_plan
    has_many :medicine_plan, dependent: :destroy
    
    validates :color, presence: true # Todo validate within 'green', 'red', or 'yellow'
    # todo validate that not both of peakflow_min/max are nullish
    validates :description, presence: true
    
    # This one's required at a minimum, an emergency situation
    validates :quick_medicine_plan, presence: true
end
