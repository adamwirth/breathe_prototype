class MedicinePlansController < ApplicationController
  before_action :set_medicine_plan, only: [:show, :edit, :update, :destroy]

  # GET /medicine_plans
  # GET /medicine_plans.json
  def index
    @medicine_plans = MedicinePlan.all
  end

  # GET /medicine_plans/1
  # GET /medicine_plans/1.json
  def show
  end

  # GET /medicine_plans/new
  def new
    @medicine_plan = MedicinePlan.new
  end

  # GET /medicine_plans/1/edit
  def edit
  end

  # POST /medicine_plans
  # POST /medicine_plans.json
  def create
    @medicine_plan = MedicinePlan.new(medicine_plan_params)

    respond_to do |format|
      if @medicine_plan.save
        format.html { redirect_to @medicine_plan, notice: 'Medicine plan was successfully created.' }
        format.json { render :show, status: :created, location: @medicine_plan }
      else
        format.html { render :new }
        format.json { render json: @medicine_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /medicine_plans/1
  # PATCH/PUT /medicine_plans/1.json
  def update
    respond_to do |format|
      if @medicine_plan.update(medicine_plan_params)
        format.html { redirect_to @medicine_plan, notice: 'Medicine plan was successfully updated.' }
        format.json { render :show, status: :ok, location: @medicine_plan }
      else
        format.html { render :edit }
        format.json { render json: @medicine_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /medicine_plans/1
  # DELETE /medicine_plans/1.json
  def destroy
    @medicine_plan.destroy
    respond_to do |format|
      format.html { redirect_to medicine_plans_url, notice: 'Medicine plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medicine_plan
      @medicine_plan = MedicinePlan.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def medicine_plan_params
      params.require(:medicine_plan).permit(:name, :dosageNum, :dosageMeasure, :frequency)
    end
end
