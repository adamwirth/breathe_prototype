class PlanZonesController < ApplicationController
  before_action :set_plan_zone, only: [:show, :edit, :update, :destroy]

  # GET /plan_zones
  # GET /plan_zones.json
  def index
    @plan_zones = PlanZone.all
  end

  # GET /plan_zones/1
  # GET /plan_zones/1.json
  def show
  end

  # GET /plan_zones/new
  def new
    @plan_zone = PlanZone.new
  end

  # GET /plan_zones/1/edit
  def edit
  end

  # POST /plan_zones
  # POST /plan_zones.json
  def create
    @plan_zone = PlanZone.new(plan_zone_params)

    respond_to do |format|
      if @plan_zone.save
        format.html { redirect_to @plan_zone, notice: 'Plan zone was successfully created.' }
        format.json { render :show, status: :created, location: @plan_zone }
      else
        format.html { render :new }
        format.json { render json: @plan_zone.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plan_zones/1
  # PATCH/PUT /plan_zones/1.json
  def update
    respond_to do |format|
      if @plan_zone.update(plan_zone_params)
        format.html { redirect_to @plan_zone, notice: 'Plan zone was successfully updated.' }
        format.json { render :show, status: :ok, location: @plan_zone }
      else
        format.html { render :edit }
        format.json { render json: @plan_zone.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plan_zones/1
  # DELETE /plan_zones/1.json
  def destroy
    @plan_zone.destroy
    respond_to do |format|
      format.html { redirect_to plan_zones_url, notice: 'Plan zone was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan_zone
      @plan_zone = PlanZone.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def plan_zone_params
      params.require(:plan_zone).permit(:color, :description, :symptoms, :peakflowMin, :peakflowMax, :controlMedicinePlan, :quickMedicinePlan, :physicalActivityPlan)
    end
end
