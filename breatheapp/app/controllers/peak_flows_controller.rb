class PeakFlowsController < ApplicationController
  before_action :set_peak_flow, only: [:show, :edit, :update, :destroy]

  # GET /peak_flows
  # GET /peak_flows.json
  def index
    @peak_flows = PeakFlow.all
  end

  # GET /peak_flows/1
  # GET /peak_flows/1.json
  def show
  end

  # GET /peak_flows/new
  def new
    @peak_flow = PeakFlow.new
  end

  # GET /peak_flows/1/edit
  def edit
  end

  # POST /peak_flows
  # POST /peak_flows.json
  def create
    @peak_flow = PeakFlow.new(peak_flow_params)

    respond_to do |format|
      if @peak_flow.save
        format.html { redirect_to @peak_flow, notice: 'Peak flow was successfully created.' }
        format.json { render :show, status: :created, location: @peak_flow }
      else
        format.html { render :new }
        format.json { render json: @peak_flow.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /peak_flows/1
  # PATCH/PUT /peak_flows/1.json
  def update
    respond_to do |format|
      if @peak_flow.update(peak_flow_params)
        format.html { redirect_to @peak_flow, notice: 'Peak flow was successfully updated.' }
        format.json { render :show, status: :ok, location: @peak_flow }
      else
        format.html { render :edit }
        format.json { render json: @peak_flow.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /peak_flows/1
  # DELETE /peak_flows/1.json
  def destroy
    @peak_flow.destroy
    respond_to do |format|
      format.html { redirect_to peak_flows_url, notice: 'Peak flow was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_peak_flow
      @peak_flow = PeakFlow.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def peak_flow_params
      params.require(:peak_flow).permit(:rating)
    end
end
