class UserCarePlansController < ApplicationController
  before_action :set_user_care_plan, only: [:show, :edit, :update, :destroy]

  # GET /user_care_plans
  # GET /user_care_plans.json
  def index
    @user_care_plans = UserCarePlan.all
  end

  # GET /user_care_plans/1
  # GET /user_care_plans/1.json
  def show
  end

  # GET /user_care_plans/new
  def new
    @user_care_plan = UserCarePlan.new
  end

  # GET /user_care_plans/1/edit
  def edit
  end

  # POST /user_care_plans
  # POST /user_care_plans.json
  def create
    @user_care_plan = UserCarePlan.new(user_care_plan_params)

    respond_to do |format|
      if @user_care_plan.save
        format.html { redirect_to @user_care_plan, notice: 'User care plan was successfully created.' }
        format.json { render :show, status: :created, location: @user_care_plan }
      else
        format.html { render :new }
        format.json { render json: @user_care_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_care_plans/1
  # PATCH/PUT /user_care_plans/1.json
  def update
    respond_to do |format|
      if @user_care_plan.update(user_care_plan_params)
        format.html { redirect_to @user_care_plan, notice: 'User care plan was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_care_plan }
      else
        format.html { render :edit }
        format.json { render json: @user_care_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_care_plans/1
  # DELETE /user_care_plans/1.json
  def destroy
    @user_care_plan.destroy
    respond_to do |format|
      format.html { redirect_to user_care_plans_url, notice: 'User care plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_care_plan
      @user_care_plan = UserCarePlan.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_care_plan_params
      params.require(:user_care_plan).permit(:triggerList, :severityClassification, :peakFlowHistory, :zones, :emergencyContacts)
    end
end
