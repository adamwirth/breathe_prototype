json.extract! peak_flow, :id, :rating, :created_at, :updated_at
json.url peak_flow_url(peak_flow, format: :json)
