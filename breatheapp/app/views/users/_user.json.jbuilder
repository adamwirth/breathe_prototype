json.extract! user, :id, :name, :loginState, :admin, :patients, :care_plan, :created_at, :updated_at
json.url user_url(user, format: :json)
