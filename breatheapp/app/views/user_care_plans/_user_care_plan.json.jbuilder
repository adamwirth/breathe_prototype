json.extract! user_care_plan, :id, :triggerList, :severityClassification, :peakFlowHistory, :zones, :emergencyContacts, :created_at, :updated_at
json.url user_care_plan_url(user_care_plan, format: :json)
