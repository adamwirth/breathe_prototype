json.extract! medicine_plan, :id, :name, :dosageNum, :dosageMeasure, :frequency, :created_at, :updated_at
json.url medicine_plan_url(medicine_plan, format: :json)
