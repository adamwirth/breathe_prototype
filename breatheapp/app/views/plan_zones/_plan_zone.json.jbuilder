json.extract! plan_zone, :id, :color, :description, :symptoms, :peakflowMin, :peakflowMax, :controlMedicinePlan, :quickMedicinePlan, :physicalActivityPlan, :created_at, :updated_at
json.url plan_zone_url(plan_zone, format: :json)
