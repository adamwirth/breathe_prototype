class CreateUserCarePlans < ActiveRecord::Migration[6.1]
  def change
    create_table :user_care_plans do |t|
      t.string 'trigger_list', array: true
      t.integer :severity_classification
      #todo later t.peak_flow 'peak_flow_history', array: true
      #todo later t.plan_zone :zones, array: true
      #todo later t.emergency_contact :emergency_contacts, array: true

      t.timestamps
    end
  end
end
