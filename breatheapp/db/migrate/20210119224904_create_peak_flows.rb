class CreatePeakFlows < ActiveRecord::Migration[6.1]
  def change
    create_table :peak_flows do |t|
      t.integer :rating

      t.timestamps
    end
  end
end
