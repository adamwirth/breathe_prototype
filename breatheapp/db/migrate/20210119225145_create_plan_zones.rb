class CreatePlanZones < ActiveRecord::Migration[6.1]
  def change
    create_table :plan_zones do |t|
      t.string :color
      t.string :description
      t.string :symptoms
      t.integer :peakflow_min
      t.integer :peakflow_max
      #todo later t.medicine_plan :control_medicine_plan
      #todo later t.medicine_plan :quick_medicine_plan
      #todo later t.medicine_plan :physical_activity_plan

      t.timestamps
    end
  end
end
