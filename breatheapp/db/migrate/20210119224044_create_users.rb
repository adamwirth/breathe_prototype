class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.boolean :login_state
      t.boolean :admin
      t.integer 'patients', array: true
      #todo later t.user_care_plans :care_plan

      t.timestamps
    end
  end
end
