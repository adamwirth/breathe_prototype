class CreateMedicinePlans < ActiveRecord::Migration[6.1]
  def change
    create_table :medicine_plans do |t|
      t.string :name
      t.float :dosage_num
      t.string :dosage_measure
      t.integer :frequency

      t.timestamps
    end
  end
end
