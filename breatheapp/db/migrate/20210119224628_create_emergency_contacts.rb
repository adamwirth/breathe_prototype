class CreateEmergencyContacts < ActiveRecord::Migration[6.1]
  def change
    create_table :emergency_contacts do |t|
      t.string :name
      t.integer :phone

      t.timestamps
    end
  end
end
