Rails.application.routes.draw do
  resources :medicine_plans
  resources :plan_zones
  resources :peak_flows
  resources :user_care_plans
  resources :users
  root 'pages#home'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
