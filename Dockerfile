# Dockerfile - Development environment
FROM ruby:2.7.2
MAINTAINER adamowirth@gmail.com
# todo optimize this later https://lipanski.com/posts/dockerfile-ruby-best-practices getting off the ground for today, need to allocate brainpower wisely


# Create breathe user (as applicable)
RUN groupadd -r breathe \
    && useradd -r -g breathe -G audio,video -d /home/docker breathe

ENV INSTALL_PATH /opt/app
RUN mkdir -p $INSTALL_PATH

# Node
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg -o /root/yarn-pubkey.gpg \
    && apt-key add /root/yarn-pubkey.gpg
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN apt-get update \
    && apt-get install -y --no-install-recommends nodejs yarn

# Rails, bundle
RUN gem install rails bundler
COPY breatheapp/Gemfile Gemfile
WORKDIR /opt/app/breatheapp
RUN bundle install

RUN chown -R breathe:breathe /opt/app
USER breathe
VOLUME ["$INSTALL_PATH/public"]
# Start unicorn server
CMD bundle exec unicorn -c config/unicorn.rb