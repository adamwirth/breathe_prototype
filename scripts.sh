#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

exit()
# not really executing this, just for syntax highlighting and cli usage for the moment

docker build \
       -t breathe \
       --build-arg USER_ID=$(id -u)  \
       --build-arg GROUP_ID=$(id -g) \
       -f Dockerfile .

# windows ps cli version of next cmd (edit to $PWD otherwise): 
# docker run -it -v ${PWD}:/opt/app breathe rails new --skip-bundle breatheapp
docker run -it -v $PWD:/opt/app breathe rails new --skip-bundle breatheapp

# For docker-compose.yml references. Should be made automatically
docker volume create --name breatheapp-postgres
docker volume create --name breatheapp-redis

# Database init
docker­-compose run --­­user "$(id ­-u):$(id -­g)" breatheapp rake db:reset
docker­-compose run --­­user "$(id ­-u):$(id -­g)" breatheapp rake db:migrate

# todo pass with -e USER or other env variables
docker­-compose run breatheapp rake db:reset
docker­-compose run breatheapp rake db:migrate

docker-­compose run breatheapp rails g controller Pages home
docker-­compose run breatheapp rails g job counter